
#define F_CPU 16000000

# include <avr/io.h>
# include <avr/iom328p.h>
# include <util/delay.h>

int main(){
	DDRA |= (1 << DDD2);
	DDRB |= (1 << DDD3);
	DDRC |= (1 << DDD4);
	DDRD |= (1 << DDD5);
		
	while(1) {				
		_delay_ms(1); // 1 ms,  maksimalna kutna brzina
		{if(DDD2) DDRI |=  (1 << DDIN1);
		if(DDD3) DDRJ |=  (1 << DDIN2);
		if(DDD4) DDRK |=  (1 << DDIN3);
		if(DDD5) DDRL |=  (1 << DDIN4);}
	}
	return 0;	
}