#define __AVR_ATmega328P__

#include <avr/io.h> 
#include <util/delay.h> 
#include <avr/interrupt.h>

#include <stdint.h> 
#include <stdio.h>  	

typedef uint16_t i16; 

uint16_t foo;

#define foo ADC4

#define  uvjet ADCIF

volatile uint8_t pin;

volatile uint16_t us_measurement;

void ir_measure() 
{		
	pin = 1;
}

void ispisi(){
	printf(" pin = %u ",pin);
	printf(" us_measurement = %u ",us_measurement);
}

void zatim( )
{
	sei();
	
	_delay_ms(100);
	
	volatile uint8_t * pADMUX;

	if ( pin ) { 
		us_measurement = (uint16_t *) pADMUX;
		ispisi( );		
	}	
}

int main (void) {        
      	
	ir_measure(); 

	zatim();
						
    return 0;                 
} 