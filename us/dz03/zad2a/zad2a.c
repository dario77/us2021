#define __AVR_ATmega328P__

#include <avr/io.h> 
#include <util/delay.h> 
#include <avr/interrupt.h>

#include <stdint.h> 
#include <stdio.h>  	

typedef uint16_t i16; 

uint16_t foo;

#define foo ADC4

#define  uvjet ADCIF

volatile uint8_t pin = 0;

void init() 
{		
	pin = 1;
}

void ispisi(){
	printf(" pin = %u ",pin);
	printf(" us_measurement = %u ",us_measurement);
}

void zatim( )
{
	 sei();
	
	_delay_ms(100);
	
	volatile uint8_t * pADMUX;

	if ( pin ) { 
		us_measurement = (uint16_t *) pADMUX;
		ispisi( );		
	}	
}

void pisi_znak(char c) {
	loop_until_bit_is_set(UCSR0A, UDRE0);
	UDR0 = c;
}

void ispisi(char *data) {
	while(*data)
		pisi_znak(*data++);
}

uint8_t analognoCitaj(uint8_t pin)
{
	uint8_t l, h;
	if (pin >= 14) pin -= 14; // allow for channel or pin numbers	
	ADMUX = (analog_reference << 6) | (pin & 0x07);	
	sbi(ADCSRA, ADSC);	
	while (bit_is_set(ADCSRA, ADSC));		
		l  = ADCL;
		h = ADCH;	
	return (h << 8) | l;
}

int main (void) {        
      	
	init(); 

	zatim();

	uint8_t napon = analognoCitaj( pin );

	uint8_t ref = 0;

	if(napon > ref){ ispisi("Iznad 1.1V");}
	else{ ispisi("Ispod 1.1V");}

    return 0;                 
} 