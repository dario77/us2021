#ifndef __AVR_ATmega328P__
#define __AVR_ATmega328P__
#endif 

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "serial.h"

volatile uint8_t b = 0;     
volatile uint8_t c = 0;     

ISR(SPI_STC_vect) 
{
    c = SPDR;  
    SPDR = b;  
}

int main() {

    DDRB |= (1 << DDB4);  
    SPCR = (1 << SPE) | (1 << SPIE); 
    sei(); 

    while (1) {
        b = 255-c;
        uart_putstr("SPI: %d"); 
        uart_putint(b);     
        uart_putchar('\n'); 
        _delay_ms(100);
    }        
    return 0;
}