#ifndef __AVR_ATmega328P__
#define __AVR_ATmega328P__
#endif 

#include <avr/io.h>
#include <util/delay.h>

#include "serial.h"

int main() {

    uart_init();

    DDRB |= (1 << DDB2) | (1 << DDB3) | (1 << DDB5);  
    DDRB &= ~(1 << DDB4);     
    
    SPCR = (1 << SPE) | (1 << MSTR) | (1 << SPR0);
     
    uint8_t x = 0; 
    uint8_t y;     

    while (x <= 255) 
    {
        SPDR = x;   
        while (!(SPSR & (1 << SPIF))); 
        y = SPDR;   
        uart_putint(x);     
        uart_putint(y);     
        uart_putchar('\n'); 
        _delay_ms(100);     
        ++x;
    }
    return 0;
}