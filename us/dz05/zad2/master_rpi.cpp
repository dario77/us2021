#include <stdio.h>
#include <unistd.h>     
#include <stdint.h>     
#include <fcntl.h>      
#include <sys/ioctl.h>  
#include <linux/spi/spidev.h> 

#ifndef SPI_PATH
#define SPI_PATH "/dev/spidev0.0"
#endif

#define SEND_UINT16 0xff 
#define RECEIVE_UINT16 0xfe 
#define SEND_UINT32 0xfd 
#define RECEIVE_UINT32 0xfc 
 
void initSPI(int& fd, uint8_t& mode) {
    if ((fd = open(SPI_PATH, O_RDWR)) < 0) {
        perror(" Error: Unable to open SPI device ! \n");
        return;
    }

    if (ioctl(fd, SPI_IOC_WR_MODE, &mode) < 0) {
        perror(" Error: Unable to open SPI device ! \n");
        return;
    }

    if (ioctl(fd, SPI_IOC_RD_MODE, &mode) < 0) {
        perror(" Error: Unable to get SPI mode ! \n");
        return;
    }

    printf(" SPI mode is: %d \n", mode);

}

void send_uint16_t(int& fd, int16_t d) 
{   
    spi_ioc_transfer send16{};          
    send16.tx_buf = (uint64_t)(&d);     
    send16.speed_hz = 1000000;          
    send16.bits_per_word = 8;           
    send16.delay_usecs = 0;             

    int stanje = ioctl(fd, SPI_IOC_MESSAGE(1), &send16);
    if (stanje < 0) {
        perror(" Error: Unable to send data in SPI ! \n");
        return;
    }
}

void send_uint32_t(int& fd, int32_t d) 
{   
    spi_ioc_transfer send32{};          
    send32.tx_buf = (uint64_t)(&d);     
    send32.speed_hz = 1000000;          
    send32.bits_per_word = 8;           
    send32.delay_usecs = 0;             

    int stanje = ioctl(fd, SPI_IOC_MESSAGE(1), &send32);
    if (stanje < 0) {
        perror(" Error: Unable to send data in SPI ! \n");
        return;
    }
}

void receive_uint16_t(int& fd, uint16_t& d) 
{   
    spi_ioc_transfer receive16{};       
    receive16.rx_buf = (uint64_t)(&d);  
    receive16.speed_hz = 1000000;       
    receive16.bits_per_word = 8;        
    receive16.delay_usecs = 0;          
    int stanje = ioctl(fd, SPI_IOC_MESSAGE(1), &receive16);   
    if (stanje < 0) {
        perror(" Error: Unable to receive data in SPI ! \n");
        return;
    }
}

void receive_uint32_t(int& fd, uint32_t& d) 
{   
    spi_ioc_transfer receive32{};       
    receive32.rx_buf = (uint64_t)(&d);  
    receive32.speed_hz = 1000000;       
    receive32.bits_per_word = 8;        
    receive32.delay_usecs = 0;          
    int stanje = ioctl(fd, SPI_IOC_MESSAGE(1), &receive32);   
    if (stanje < 0) {
        perror(" Error: Unable to receive data in SPI ! \n");
        return;
    }
}

int main() {
    int fd;                 
    uint8_t mode;           
    initSPI(fd, mode);      

    uint16_t s16 = (uint16_t) SEND_UINT16; 
    uint32_t s32 = (uint32_t) SEND_UINT32;   
    uint16_t r16 = (uint16_t) RECEIVE_UINT16; 
    uint32_t r32 = (uint32_t) RECEIVE_UINT32; 
        
    while (1){ 
        send_uint16_t(fd, s16);                
        send_uint32_t(fd, s32);
        receive_uint16_t(fd, r16);
        receive_uint32_t(fd, r32);
        printf(" [uint16_t] Sent: %u \n", s16);        
        printf(" [uint32_t] Sent: %u \n", s32);        
        printf(" [uint16_t] Received: %u \n", r16);        
        printf(" [uint32_t] Received: %u \n", r32);      
        s16 =  (uint16_t) s16*3;        
        s32 = (uint32_t) s32*4;        
        usleep(100000); 
        if ( s16 == 0 | r16 == 0 | s32 == 0 | r32 == 0 ) break;
    }
    close(fd); 
    return 0;
}