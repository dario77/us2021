#ifndef __AVR_ATmega328P__
#define __AVR_ATmega328P__
#endif 

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "serial.h"

#define SEND_UINT16 0xff 
#define RECEIVE_UINT16 0xfe 
#define SEND_UINT32 0xfd 
#define RECEIVE_UINT32 0xfc 

volatile uint16_t b = 0;     
volatile uint16_t c = 0;     
volatile uint32_t d = 0;     
volatile uint32_t e = 0;     

ISR(SPI_STC_vect) 
{
    c = SPDR;  
    SPDR = b;  
    d = SPDR;  
    SPDR = e;  
}

int main() {
    DDRB |= (1 << DDB4);  
    SPCR = (1 << SPE) | (1 << SPIE); 

    sei(); 

    while (1) {
        b = (uint16_t) 4*c;
        e = (uint32_t) 5*d;

        uart_putstr("[uint16_t] SPI Sent: %d \n"); 
        uart_putint(b);     
        uart_putstr("[uint32_t] SPI Sent: %u \n"); 
        uart_putint(e);     
        uart_putstr("[uint16_t] SPI Received: %d \n"); 
        uart_putint(c);     
        uart_putstr("[uint32_t] SPI Received: %u \n"); 
        uart_putint(d);     

        _delay_ms(100);
    }  
    return 0;
}