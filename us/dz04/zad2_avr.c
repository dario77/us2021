// Ugrađeni sustavi
// Zadaća 4 Zadatak 2 zad2_avr.c
// Dario Jurić

#define __AVR_ATmega328P__

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

volatile uint16_t d = 0;
void send2byte(uint16_t b) {
    while (!(UCSR0A & (1 << UDRE0)));
    UDR0 = b;
}

volatile uint16_t c = 0;
ISR(USART_RX_vect) {
    c = UDR0;
    send2byte(c);    
    _delay_ms(100);   
}

uint16_t receive2bytes() {
    while (!(UCSR0A & (1 << RXC0)));
    return UDR0;
}

void send2bytes(uint16_t b) {
    while (!(UCSR0A & (1 << UDRE0)));
    UDR0 = b;
}

int main() {
    
    DDRB |= ( 1 << DDB5 );
    
    UCSR0C = (1 << UCSZ01) | (1 << UCSZ00);

    UCSR0B = (1 << TXEN0) | (1 << RXEN0); 

    UBRR0 = 103;

    UCSR0B |= (1 << RXCIE0); 

    sei();
    
    return 4;
}

