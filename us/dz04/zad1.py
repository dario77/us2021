# Ugrađeni sustavi
# Zadaća 4 Zadatak 1
# Dario Jurić

import Rpi.GPIO as GPIO
import time
from gpiozero import DistanceSensor

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO_PREK = 18
GPIO_ECHO = 24

GPIO.setup(GPIO_PREK, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)

def daljina( ):
	GPIO.output(GPIO_PREK,True)
	time.sleep(0.001)
	GPIO.output(GPIO_PREK,False)
	StartVrijeme = time.time()
	StopVrijeme = time.time()
	while GPIO.input(GPIO_ECHO) == 1:
   		StartVrijeme = time.time()
	while GPIO.input(GPIO_ECHO) == 0:
   		StopVrijeme = time.time()
	VrijemeT = StopVrijeme – StartVrijeme
	ds = VrijemeT*17150
	return ds

if __name__ == "main":
	traje = 1
	d = 0
	try: 
		while traje < 1000 :
			d = daljina()
			print(" Distance: %f cm" % d)
			time.sleep(0.25)
			traje += 1
	except KeyboardInterrupt:
		print(" Kraj mjerenja udaljenosti. ")
		GPIO.cleanup()
