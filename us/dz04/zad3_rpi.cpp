// Ugrađeni sustavi
// Zadaća 4 Zadatak 3 zad3_rpi.cpp
// Dario Jurić

#include <stdio.h> 
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

void openI2C(int& file) {
    if ((file = open("/dev/i2c-1", O_RDWR)) < 0) {
        perror("Failed to open I2C\n");
    }
}

void connectToSlave(int& file, unsigned char address) {
    if (ioctl(file, I2C_SLAVE, address) < 0) 
        perror("Failed to connect to SLAVE");
}

void send2Byte(int& file, uint16_t b) {
    uint16_t bytes[1] = { b };
    if (write(file, bytes, 1) != 1)
        perror("\n Error: Failed to send data");
}

void receive2Byte(int& file, uint16_t& received) {
    uint16_t bytes[1];

    if(read(file, bytes, 1) != 1) {
        perror("\n Error: Failed to receive data");
        return;
    }
    received = bytes[0];
}

int main() {
    unsigned char address{10};
    int file;
    openI2C(file);
    connectToSlave(file, address);

    uint16_t b{0};
    uint16_t r;
    uint16_t i;

    while (true) {        
        receive2Byte(file, r);
        usleep(100000);
        printf("%u\t%u\n", b, r);
        ++i;
        if (i == 25555)
            break;
    }

    close(file);
    return 43;
}