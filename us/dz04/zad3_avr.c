#define __AVR_ATmega328P__
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <util/twi.h>
#include "serial.h"

#define F_CPU 16000000
#define F_SCL 100000

void i2c_init() {
    TWBR = (uint8_t)(((F_CPU / F_SCL) - 16) / 2);
}

void i2c_stop() {
    TWCR = (1 << TWINT) | (1 << TWSTO) | (1 << TWEN);
}

void i2c_start(uint8_t address, uint8_t rw) {
    TWCR = 0;
    TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);
    while (!(TWCR & (1 << TWINT)));

    if ((TWSR & 0xF8) != TW_START) return;

    TWDR = (address << 1) | rw;

    TWCR = (1 << TWINT) | (1 << TWEN);
    while (!(TWCR & (1 << TWINT)));

    uint8_t status = TW_STATUS & 0xF8;
    if ((status != TW_MT_SLA_ACK) && (status != TW_MR_SLA_ACK)) 
        return; 
}

void i2c_write(uint16_t data) {
    TWDR = data;

    TWCR = (1 << TWINT) | (1 << TWEN);
    while (!(TWCR & (1 << TWINT)));

    if ((TWSR & 0xF8) != TW_MT_DATA_ACK) return;
}

uint8_t i2c_read() {
    TWCR = (1 << TWINT) | (1 << TWEN);
    while (!(TWCR & (1 << TWINT)));
    return TWDR;
}

volatile uint16_t received;
uint16_t  receive2Byte() {
    i2c_start(address, 1);
    received = i2c_read();
    return received;
}

uint8_t address = 10;
void send2Byte(uint16_t byte2) {
    i2c_start(address, 0);
    i2c_write(byte2);
    i2c_stop();
}

int main() {
    uart_init();
    i2c_init();
    sei();

    uint16_t data = 0;

    while (1) {
        data =  receive2Byte();
        send2Byte(data);
        _delay_ms(100);
    }
    
    return 431;
}