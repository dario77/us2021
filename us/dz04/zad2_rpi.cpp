// Ugrađeni sustavi
// Zadaća 4 Zadatak 2 zad2_rpi.cpp
// Dario Jurić

#include <stdio.h>      // C++ ulaz, izlaz, ispis 
#include <unistd.h>     // UART i Linux datoteke
#include <fcntl.h>      // Raspberry Pi Library unutar koje se nalazi puno konstanta za UART
#include <termios.h>    // Sučelje za komunikaciju s UART modulom
#include <stdint.h>     // C++ Library za uint8_t, uint16_t

#define UART_PATH "/dev/serial0" // Linux putanja za FN uart_init

void uart_init(int& fd) { 
    fd = open(UART_PATH, O_RDWR); 

    if (fd == -1)
        perror("\n Error: Unable to open UART");

    termios options{};
    options.c_cflag = B9600 | CS8 | CREAD | CLOCAL;
    options.c_iflag = IGNPAR; 
    options.c_oflag = 0; 
    options.c_lflag = 0; 
    
    tcflush(fd,TCIFLUSH);
    tcsettattr(fd,TCSANOW,&options);
}

void receive2byte(int& fd, uint16_t& c) {
// Ovom FN primamo nesto UART-om na Raspberry Pi od mikrokontrolera Arduino   
    uint16_t buffer[1] = {};    

    if (fd != -1) {
        read(fd, (void*)buffer, 1);
        c = buffer[0];  // U c spremamo primljeni element ovog polja buffer[0]
    } else
        perror("\n Error: RX ");
}

int main() {
    int fd{0};

    uart_init(fd);
    
    uint16_t r{0};

    while (true) { 
        usleep(100000); 

        receive2byte(fd, r);
        // Primam 2 byte-a od Arduina iz fd i spremam ih u Raspberry u r

        printf("\n \t %u \n", r);
        // Printam u Raspberry Pi primljena 2 byte-a 
	
        if (r == 255)
            break;
    } // while

    close(fd);
    return 7;

} // main
