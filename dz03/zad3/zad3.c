#define __AVR_ATmega328P__

#ifndef SERIAL_H_
#define SERIAL_H_
#endif

#ifndef BAUD
#define BAUD 9600
#endif

#include <avr/io.h> 
#include <util/delay.h> 
#include <avr/interrupt.h>
#include <avr/sfr_defs.h>
#include <util/setbaud.h>

#include <stdlib.h>
#include <stdint.h> 
#include <stdio.h> 

void motor_init(){
	DDRB |= ( 1 << DDB1 | 1 << DDB2 );
	TCCR1B |= (0 << CS10) | (0 << CS10 ) | (1 << CS12 );
	TCCR1A |= (1 << COM1A1) | (1 << COM1B1 ) | (1 << CS12 );
	TCCR1A |= (1 << WGM11) | (1 << WGM10);
	TCCR1B |= (0 << WGM12) | (1 << WGM10);
}

volatile uint8_t A = 1; // Enkoder - stanje ( trenutno )
volatile uint8_t B = 1; // Enkoder - stanje ( trenutno )

volatile uint8_t A_old = 0; // Enkoder - stanje ( arhiva )
volatile uint8_t B_old = 0; // Enkoder - stanje ( arhiva )

volatile int64_t tick = 1000; // tick

void encoder_init() {
	DDRD &= ~(1 << DDD2 ); // D2 := Ulaz 
	DDRD &= ~(1 << DDD3 ); // D3 := Ulaz 
	
	EICRA |= (1 << ISC00 ) | (1 << ISC10 );
	EIMSK |= (1 << INT0 ) | (1 << INT1 );
	
	sei(); // Bitna fn za interrapte
}

ISR( INT0_vect ) // C1 <> A <> Interrupt za A
{
	A = PIND & (1 << PIND2) ? 1 : 0;
	B = PIND & (1 << PIND3) ? 1 : 0;
	
	if( A_old == 0 && B_old == 0 && A == 1 && B == 0 ) ++tick; 	
	if( A_old == 1 && B_old == 0 && A == 1 && B == 1 ) ++tick; 	
	if( A_old == 1 && B_old == 1 && A == 0 && B == 1 ) ++tick;	
	if( A_old == 0 && B_old == 1 && A == 0 && B == 0 ) ++tick;
	
	if( A == 0 && B == 0 && A_old == 1 && B_old == 0 ) --tick; 
	if( A == 1 && B == 0 && A_old == 1 && B_old == 1 ) --tick; 	
	if( A == 1 && B == 1 && A_old == 0 && B_old == 1 ) --tick;	
	if( A == 0 && B == 1 && A_old == 0 && B_old == 0 ) --tick;
	
	A_old = A; 
}

ISR( INT1_vect ) // C2 <> B <> Interrupt za B
{	
	B = PIND & (1 << PIND3) ? 1 : 0;
	
	if( A_old == 0 && B_old == 0 && A == 1 && B == 0 ) ++tick; 	
	if( A_old == 1 && B_old == 0 && A == 1 && B == 1 ) ++tick; 	
	if( A_old == 1 && B_old == 1 && A == 0 && B == 1 ) ++tick;	
	if( A_old == 0 && B_old == 1 && A == 0 && B == 0 ) ++tick;
	
	if( A == 0 && B == 0 && A_old == 1 && B_old == 0 ) --tick; 
	if( A == 1 && B == 0 && A_old == 1 && B_old == 1 ) --tick; 	
	if( A == 1 && B == 1 && A_old == 0 && B_old == 1 ) --tick;	
	if( A == 0 && B == 1 && A_old == 0 && B_old == 0 ) --tick;
	
	B_old = B; 
}

void uart_putchar(char c) {
	loop_until_bit_is_set(UCSR0A, UDRE0);
	UDR0 = c;
}

void uart_putstr(char *data) {
	while(*data)
		uart_putchar(*data++);
}

void uart_putint(int32_t n)  {
	char str[10];
	itoa(n, str, 10);
	uart_putstr(str);
	uart_putchar('\n');
}

void uart_putstanje(int64_t n)  {
	itoa(n, str, 10);
	uart_putstr(str);
	uart_putchar('\n');
}

int zad3() { 
	// zad3 main.c
	
	uart_init();
	
	encoder_init();
	
	DDRB |= (1<<DDB1) | (1<<DDB2); 
	 
	PORTB |= (1<<PORTB1); 
	
	
	if( A == 1 ){
		if ( tick == 860 ) { tick = tick + 430; uart_putint( tick ); OCR1A += 1.5; }		
		if ( tick == 1290 ) { tick = tick - 430; uart_putint( tick ); OCR1A -= 1.5; }				
		--A;
	}	// if
	
	if( B == 1 ){
		while(1){		
			_delay_ms(2000);   
		
			if ( tick == 1000 ) { tick = 1500; uart_putstanje( tick );}
			if ( tick == 1500 ) { tick = 1000; uart_putstanje( tick );}
						
		} // while	
	} // if
	
	return 0;
	
} // zad3	

